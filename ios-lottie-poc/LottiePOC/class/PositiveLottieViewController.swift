//
//  LottieViewController.swift
//  LottiePOC
//
//  Created by Guilherme Zanellato Oliveira on 27/07/18.
//  Copyright © 2018 Guilherme Zanellato Oliveira. All rights reserved.
//

import UIKit
import Lottie

class PositiveLottieViewController: UIViewController {

    @IBOutlet weak var lottieView: UIView!
    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var midButton: UIButton!
    @IBOutlet weak var mainView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        self.loadLottie()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func loadLottie () {
        let animationView = LOTAnimationView()
        animationView.setAnimation(named: "positive_lottie")
        animationView.frame = self.lottieView.frame
        animationView.center = self.lottieView.center
        animationView.contentMode = UIViewContentMode.scaleAspectFill
        animationView.loopAnimation = true
        animationView.play()
        
        self.lottieView.addSubview(animationView)
    }
}
