package animationpoc.itau.br.itauanimationpoc;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.airbnb.lottie.LottieAnimationView;

public class TopCropLottieAnimationView extends LottieAnimationView {

    public TopCropLottieAnimationView(Context context) {
        super(context);
        init();
    }

    public TopCropLottieAnimationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TopCropLottieAnimationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        recomputeImgMatrix();
    }

    @Override
    protected boolean setFrame(int l, int t, int r, int b) {
        recomputeImgMatrix();
        return super.setFrame(l, t, r, b);
    }

    private void init() {
        setScaleType(ScaleType.MATRIX);
    }

    private void recomputeImgMatrix() {

        final Drawable drawable = getDrawable();
        if (drawable == null) {
            return;
        }

        final Matrix matrix = getImageMatrix();

        float scale;
        final int viewWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        final int viewHeight = getHeight() - getPaddingTop() - getPaddingBottom();
        final int drawableWidth = drawable.getIntrinsicWidth();
        final int drawableHeight = drawable.getIntrinsicHeight();

        int x = 0;
        int y = 0;
        if (drawableWidth * viewHeight > drawableHeight * viewWidth) {
            scale = (float) viewHeight / (float) drawableHeight;
            x = (int) (viewWidth - (drawableWidth * scale));
        } else {
            scale = (float) viewWidth / (float) drawableWidth;
            y = (int) (viewHeight - (drawableHeight * scale));
        }

        matrix.setScale(scale, scale);
        matrix.postTranslate(x,y);
        setImageMatrix(matrix);
    }
}