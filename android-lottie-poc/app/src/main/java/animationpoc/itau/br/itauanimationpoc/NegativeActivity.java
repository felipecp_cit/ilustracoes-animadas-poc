package animationpoc.itau.br.itauanimationpoc;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.OnCompositionLoadedListener;

public class NegativeActivity extends AppCompatActivity {
    private LottieAnimationView animationView;
    private OnCompositionLoadedListener loadedListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
        setContentView(R.layout.animation_activity);
        animationView = findViewById(R.id.animation_view);
        animationView.setBackgroundResource(R.drawable.negative_background);
        loadedListener = new OnCompositionLoadedListener() {
            @Override
            public void onCompositionLoaded(@Nullable LottieComposition composition) {
                assert composition != null;
                animationView.setComposition(composition);
            }
        };
        animationView.post(new Runnable() {
            @Override
            public void run() {
                LottieComposition.Factory.fromRawFile(NegativeActivity.this, R.raw.negative, loadedListener);
                animationView.playAnimation();
            }
        });
    }

    @Override
    protected void onPause() {
        if(animationView.isAnimating()) {
            animationView.pauseAnimation();
        }
        super.onPause();
    }
}
